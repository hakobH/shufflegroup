﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleInGroups
{
    class Student
    { 
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Mail { get; set; }

        public void Show()
        {
            Console.WriteLine($"Name: {Name}, Last Name: {LastName}, Age: {Age}, Email: {Mail}");
        }
    }
}
