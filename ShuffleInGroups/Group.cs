﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleInGroups
{
    class Group
    {
        public string Name { get; set; }
        public Teacher teacher;
        public List<Student> students;

        public void AddStudent(Student student)
        {
            students.Add(student);
        }
    }
}
