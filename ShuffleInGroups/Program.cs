﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleInGroups
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> studentList = GenerateStudents(17);
            List<Teacher> teachers = GenerateTeachers(3);
            List<Group> groups = SeperateGroups(studentList, teachers);

            //Print(studentList);
            Console.WriteLine("-------------------------------");
            Print(groups);

            Console.ReadLine();
        }
        static List<Teacher> GenerateTeachers(int count)
        {
            List<Teacher> teachers = new List<Teacher>(count);
            for (int i = 0; i < count; i++)
            {
                Teacher st = new Teacher
                {
                    Name = "A" + i,
                    lastName = "A" + i + "yan",                 
                };
                teachers.Add(st);
            }
            return teachers;
        }
        static List<Student> GenerateStudents(int count)
        {
            List<Student> studentList = new List<Student>();
            for (int i = 0; i < count; i++)
            {           
                Student st = new Student
                {
                    Name= "A" + i,
                    LastName = "A" + i + "yan",
                    Age = i,
                    Mail = $"{"A" + i}.{"A" + i + "yan"}@gmail.com"
                };
                studentList.Add(st);
            }
            return studentList;
        }
        static void Print(Group group)
        {           
            foreach (Student item in group.students)
            {
                item.Show();
            }
        }
        static void Print(List<Group> groupList)
        {
            for (int i = 0; i < groupList.Count; i++)
            {
                Console.WriteLine($"{i + 1} Group");
                Console.WriteLine($"Group Name: {groupList[i].Name}");
                Console.WriteLine($"Teacher: {groupList[i].teacher.Name} {groupList[i].teacher.lastName}");
                Print(groupList[i]);
                Console.WriteLine("-------------------------------------");
            }
        }
        static List<Student> Copy(List<Student> studentList)
        {
            List<Student> NewStudentList = new List<Student>(studentList.Count);
            for (int i = 0; i < studentList.Count; i++)
            {
                NewStudentList.Add(studentList[i]);
            }
            return NewStudentList;
        }
        static Group CreateGroup(List<Student> source, int count,List<Teacher> teacher)
        {
            Group group = new Group();
            List<Student> students = new List<Student>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                int index = rnd.Next(0, source.Count);
                students.Add(source[index]);
                source.RemoveAt(index);
            }
            group.students = students;
            int randInt = rnd.Next(0, teacher.Count);
            group.teacher = teacher[randInt];
            teacher.RemoveAt(randInt);
            group.Name = $"Group__{randInt}";
            return group;
        }
        static List<Group> SeperateGroups(List<Student> studentList, List<Teacher> teacher)
        {
            int groupsCount = teacher.Count;
            if (groupsCount <= 0)
            {              
                return null;
            }

            studentList = Copy(studentList);
            List<Group> groupedStudents = new List<Group>(groupsCount);

            int groupLength = studentList.Count / groupsCount;
            int mnacord = studentList.Count - (groupLength * groupsCount);

            for (int i = 0; i < groupsCount; i++)
            {
                groupedStudents.Add(CreateGroup(studentList, groupLength,teacher));
            }
            for (int i = 0; i < mnacord; i++)
            {
                groupedStudents[groupedStudents.Count - i - 1].AddStudent(studentList[i]);
            }
            return groupedStudents;
        }
    }
}
